#! /usr/bin/env python

import sys

if sys.version_info >= (3, 0):
    print("\nThe script doesn't support python 3. Please use python 2.7+\n")
    sys.exit(1)

import os
import getopt
import re
import calendar
import time
import json
import datetime
from youtrackutils import csvClient
import youtrackutils.csvClient.youtrackMapping
from youtrackutils.csvClient.client import Client
from youtrack.youtrackImporter import *
from youtrack import User, Comment, Link
from youtrack.connection import Connection
from youtrack.sync.links import LinkImporter
from youtrack import Project
from xml.dom.minidom import parseString





def usage():
    basename = os.path.basename(sys.argv[0])

    print("""
Usage:
    %s [OPTIONS] project_name project_id user_name [youtrack_url]

    issues_file    Issues csv file
    youtrack_url   YouTrack base URL
    
    

Options:
    -h,  Show this help and exit
    -T TOKEN_FILE,
         Path to file with permanent token
    -t TOKEN,
         Value for permanent token as text
    -u LOGIN,
         YouTrack user login to perform import on behalf of
    -p PASSWORD,
         YouTrack user password

Examples:


    Create youtrack project:

    $ %s -T token  ProjectName ProjectId UserName https://yourCompany.myjetbrains.com/youtrack



""" % (basename, basename))

def check_file_and_save(filename, params, key):
    try:
        params[key] = os.path.abspath(filename)
    except (OSError, IOError) as e:
        print("Data file is not accessible: " + str(e))
        print(filename)
        sys.exit(1)


def connect(params):
  token = params.get('token')
  if not token and 'token_file' in params:
      try:
          with open(params['token_file'], 'r') as f:
              token = f.read().strip()
      except (OSError, IOError) as e:
          print("Cannot load token from file: " + str(e))
          sys.exit(1)
  if token:
      target = Connection(params['target_url'], token=token)
  elif 'login' in params:
      target = Connection(params['target_url'],
                          params.get('login', ''),
                          params.get('password', ''))
  else:
      print("You have to provide token or login/password to import data")
      sys.exit(1)
  return target

def createProject(params):
  target = connect(params)
  try:
    target.getProject(params['project_id'])
  except:
    target.createProject(generateProject(params["project_name"], params["project_id"], params['user_name']))

def generateProject(name,id,creator):
  xml = """<project name="%s" id="%s" lead="%s" \
          startingNumber="1" \
        />""" %(name,id,creator)
  xml = parseString(xml)
  project = Project(xml=xml)
  return project


def main():
  try:
    params = {}
    opts, args = getopt.getopt(sys.argv[1:], 'hu:p:t:T:')
    for opt, val in opts:
        if opt == '-h':
            usage()
            sys.exit(0)
        elif opt == '-u':
            params['login'] = val
        elif opt == '-p':
            params['password'] = val
        elif opt == '-t':
            params['token'] = val
        elif opt == '-T':
            check_file_and_save(val, params, 'token_file')

    params['project_name'] = args[0]
    params['project_id'] = args[1]
    params['user_name'] = args[2]
    params['target_url'] = args[3]

    createProject(params)


  except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
  except (ValueError, KeyError, IndexError):
    print("Bad arguments")
    usage()
    sys.exit(1)


if __name__ == "__main__":
  main()
